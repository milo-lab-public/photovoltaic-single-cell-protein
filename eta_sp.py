import pandas as pd
import matplotlib.pyplot as plt
import sympy
import numpy as np
from uncertainties import ufloat


class ErrorPropogation(object):
        
    def __init__(self):
        self._variables = []

    def propagate_error(self, expression: sympy.Expr) -> sympy.Expr:
        # calculate the partial derivative of η_SP as a function of each one of the variables:
        derivatives = sympy.simplify(sympy.derive_by_array(expression, self._value_symbols))

        # evaluate the derivative at the chosen mean value of η_SP, and use these values in the
        # expression for the uncertainty of η_SP (using error propagation).
        sum_of_squares = []
        for derivative, s_mu, s_sigma in zip(derivatives, self._value_symbols, self._error_symbols):
            sum_of_squares.append(derivative**2 * s_sigma**2)
        return sympy.sqrt(sum(sum_of_squares))

    def create_subs(self, **kwargs):
        subs = {}
        for i, v in enumerate(self._variables):
            subs[self._value_symbols[i]] = kwargs[v].nominal_value
            subs[self._error_symbols[i]] = kwargs[v].std_dev
        return subs

class ETA_SP(ErrorPropogation):
        
    def __init__(self):
        # pre-calculate the uncertainty funtion
        self._variables = ["eta_pv", "eta_ec", "eta_bio", "eta_prod", "theta_dac", "theta_bio", "theta_nut", "theta_ppr"]
        self._value_symbols = sympy.symbols("η_pv η_ec η_bio η_prod θ_dac θ_bio θ_nut θ_ppr")
        self._error_symbols = sympy.symbols("Δη_pv Δη_ec Δη_bio Δη_prod Δθ_dac Δθ_bio Δθ_nut Δθ_ppr")
        self._eta_star = sympy.parse_expr("1 / (1 + η_ec * η_bio * (θ_dac + θ_bio + θ_nut + θ_ppr))")    
        self._eta_star_uncertainty = self.propagate_error(self._eta_star)
        self._eta_sp = sympy.parse_expr("η_pv * η_ec * η_bio * η_prod / (1 + η_ec * η_bio * (θ_dac + θ_bio + θ_nut + θ_ppr))")    
        self._eta_sp_uncertainty = self.propagate_error(self._eta_sp)
            
    def calc_eta_star(self, **kwargs):
        subs = self.create_subs(**kwargs)
        value = self._eta_star.evalf(subs=subs)
        error = self._eta_star_uncertainty.evalf(subs=subs)
        return ufloat(value, error)

    def calc_eta_sp(self, **kwargs):
        subs = self.create_subs(**kwargs)
        value = self._eta_sp.evalf(subs=subs)
        error = self._eta_sp_uncertainty.evalf(subs=subs)
        return ufloat(value, error)


class YIELD_SB(ErrorPropogation):

    def __init__(self):
        # pre-calculate the uncertainty funtion
        self._variables = ["eta_pv", "eta_bio", "eta_prod", "theta_bio", "theta_nut", "theta_ppr", "theta_cult", "theta_extrc", "yield_sc", "irradiance"]
        self._value_symbols = sympy.symbols("η_pv η_bio η_prod θ_bio θ_nut θ_ppr θ_cult θ_extrc Y_se irrad")
        self._error_symbols = sympy.symbols("Δη_pv Δη_bio Δη_prod Δθ_bio Δθ_nut Δθ_ppr Δθ_cult Δθ_extrc ΔY_se Δ_irrad")
        self._y_sb_scp = sympy.parse_expr("Y_se * η_bio * η_prod / ( (Y_se * (θ_nut + θ_bio + θ_ppr) * η_bio + θ_cult + θ_extrc) / (η_pv * irrad * (1.576427 - 3.516161E-4  * irrad)) + 1)")
        self._y_sb_uncertainty = self.propagate_error(self._y_sb_scp)
    
    def calc_y_sb_scp(self, **kwargs):
        subs = self.create_subs(**kwargs)
        value = self._y_sb_scp.evalf(subs=subs)
        error = self._y_sb_uncertainty.evalf(subs=subs)
        return ufloat(value, error)